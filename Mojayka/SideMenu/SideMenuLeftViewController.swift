//
//  SideMenuLeftViewController.swift
//  Mojayka
//
//  Created by ifau on 09/07/17.
//  Copyright © 2017 avium. All rights reserved.
//

import UIKit

class SideMenuLeftViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    weak var delegate: LeftMenuDelegate!
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 60
        self.view.backgroundColor = Constants.blueColor()
        updateTableInsets()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return delegate.numberOfItems()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        cell.selectionStyle = .none
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.text = delegate.titleForItemAtIndex(index: indexPath.row)
        cell.textLabel?.font = UIFont(name: "HelveticaNeue-Medium", size: 15)!
        cell.backgroundColor = indexPath.row == delegate.selectedIndex() ? Constants.orangeColor() : UIColor.clear
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
        delegate.didSelectItemAtIndex(index: indexPath.row)
    }
    
    func reload()
    {
        tableView.reloadData()
    }
    
    func updateTableInsets()
    {
        let headerHeight = (self.tableView.frame.size.height - (tableView.rowHeight * CGFloat(delegate.numberOfItems()))) / 2;
        
        self.tableView.contentInset = UIEdgeInsetsMake(headerHeight, 0, 0, 0);
    }
    
    @IBAction func aboutAviumButtonPressed(_ sender: Any)
    {
        delegate.aboutAviumPressed()
    }
}
