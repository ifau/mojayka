//
//  SideMenuRootViewController.swift
//  Mojayka
//
//  Created by ifau on 09/07/17.
//  Copyright © 2017 avium. All rights reserved.
//

import UIKit
import LGSideMenuController

protocol LeftMenuDelegate: class
{
    func numberOfItems() -> Int
    func titleForItemAtIndex(index: Int) -> String
    func didSelectItemAtIndex(index: Int)
    func selectedIndex() -> Int
    func aboutAviumPressed()
}

class SideMenuRootViewController: LGSideMenuController, LeftMenuDelegate
{
    fileprivate let identifiers = ["HotToursVC", "SelectTourVC", "", "ContactsVC", "AboutVC"]
    fileprivate let titles = ["Горящие туры", "Оставить заявку на тур", "Живое сообщество", "Контакты", "О компании"]
    fileprivate let navTitles = ["Горящие туры", "Оставить заявку", "", "Контакты", "О компании"]
    
    fileprivate var currentIndex = -1;
    fileprivate var leftVC: SideMenuLeftViewController!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        leftVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "leftMenuVC") as! SideMenuLeftViewController
        leftVC.delegate = self
        
        self.leftViewPresentationStyle = .slideBelow
        self.leftViewController = leftVC
        self.leftViewWidth = 240
        didSelectItemAtIndex(index: 0)
    }
    
    func numberOfItems() -> Int
    {
        return titles.count
    }
    
    func titleForItemAtIndex(index: Int) -> String
    {
        return titles[index]
    }
    
    func didSelectItemAtIndex(index: Int)
    {
        if (index == 2)
        {
            UIApplication.shared.openURL(NSURL(string: "http://goo.gl/PQN9xJ")! as URL)
            return;
        }
        
        if (index != currentIndex)
        {
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifiers[index])
            let navigationController = UINavigationController(rootViewController: viewController)
            let menuItem = UIBarButtonItem(image: UIImage(named: "IconMenu")!, style: .plain, target: self, action: #selector(SideMenuRootViewController.menuButtonPressed))
            
            viewController.navigationItem.leftBarButtonItem = menuItem
            viewController.navigationItem.title = navTitles[index]
            
            self.rootViewController = navigationController
            currentIndex = index
            
            leftVC.reload()
            self.hideLeftView(animated: true)
        }
    }
    
    func selectedIndex() -> Int
    {
        return currentIndex
    }
    
    func menuButtonPressed()
    {
        showLeftViewAnimated()
    }
    
    func aboutAviumPressed()
    {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AviumVC")
        let navigationController = UINavigationController(rootViewController: viewController)
        present(navigationController, animated: true, completion: nil)
    }
}
