//
//  SegmentSwitchTableViewCell.swift
//  tuiapp
//
//  Created by ifau on 16/03/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class SegmentSwitchTableViewCell: UITableViewCell
{
    @IBOutlet weak var segmentSwitch: AnimatedSegmentSwitch!
    @IBOutlet var widthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        segmentSwitch.backgroundColor = UIColor.clear
        segmentSwitch.selectedTitleColor = Constants.orangeColor()
        segmentSwitch.titleColor = Constants.blueColor()
        segmentSwitch.thumbColor = UIColor.clear
        segmentSwitch.font = UIFont(name: "HelveticaNeue-Bold", size: 12.0)
    }
}
