//
//  HotToursViewController.swift
//  tuiapp
//
//  Created by ifau on 23/04/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import Kingfisher
import STPopup

class HotToursViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    fileprivate var mtours: HotTours2Response?
    fileprivate var filteredTours: [HT2Tour]?
    fileprivate var selectedSegmentIndex = 0
    @IBOutlet var tableView: UITableView!
    @IBOutlet var filtersButton: UIButton!
    
    var shouldApplyFilters = false
    {
        didSet
        {
            requestTours()
        }
    }
    
    fileprivate var filtersDatasource: HotToursFiltersDataSource?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        filtersButton.layer.cornerRadius = 25
        filtersButton.isEnabled = false
        filtersButton.alpha = 0
        
        tableView.register(UINib(nibName: "HotTourTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        tableView.register(UINib(nibName: "SegmentSwitchTableViewCell", bundle: nil), forCellReuseIdentifier: "Header")
        tableView.dataSource = self
        tableView.delegate = self
        
        let items = ["Все", "Luxury Promo", "Пляж", "Выходные", "Горные лыжи", "Оздоровление"]
        let menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, title: items.first!, items: items as [AnyObject])
        menuView.cellHeight = 50
        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = Constants.orangeColor()
        menuView.cellTextLabelColor = UIColor.white
        menuView.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 17)
        menuView.cellTextLabelAlignment = .center
        menuView.arrowPadding = 15
        menuView.animationDuration = 0.5
        menuView.maskBackgroundColor = UIColor.black
        menuView.maskBackgroundOpacity = 0.3
        menuView.didSelectItemAtIndexHandler =
            { [unowned self] (indexPath: Int) -> () in
                
                self.didSelectMenuItemIndex(index: indexPath)
        }
        
        self.navigationItem.titleView = menuView
        
        requestTours()
    }
    
    @IBAction func filtersButtonPressed(_ sender: AnyObject)
    {
        if (self.filtersDatasource == nil)
        {
            self.filtersDatasource = HotToursFiltersDataSource()
        }
        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HotToursFiltersVC") as! HotToursFiltersViewController
        viewController.datasource = self.filtersDatasource
        viewController.toursVC = self
        
        let popupController = STPopupController(rootViewController: viewController)
        popupController.present(in: self)
    }
    
    func requestTours()
    {
        let session = URLSession.shared
        var str = "https://mtours.ru/api/hot-tours2.json?key=\(apiKey)"
        
        if (shouldApplyFilters && filtersDatasource != nil)
        {
            str += filtersDatasource!.filtersURLParams()
        }
        
        let url = URL(string: str)!
        let request = URLRequest(url: url)
        
        tableView.alpha = 0.0
        showLoadingIndicator()
        showStatusBarLoadingIndicator()
        
        let task = session.dataTask(with: request, completionHandler: { [weak self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            if let strongSelf = self
            {
                if error == nil && data != nil
                {
                    DispatchQueue.main.async(execute: {
                        strongSelf.hideLoadingIndicator()
                        
                        let jsonDictionary = try! JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                        strongSelf.mtours = HotTours2Response(fromDictionary: jsonDictionary as! [String : Any])
                        strongSelf.filteredTours = strongSelf.mtours?.tours
                        strongSelf.tableView.reloadData()
                        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations:
                        {
                            strongSelf.filtersButton.alpha = 1.0
                            strongSelf.tableView.alpha = 1.0
                        },
                        completion:
                        { (Bool) -> Void in
                            
                            strongSelf.filtersButton.isEnabled = true
                            hideStatusBarLoadingIndicator()
                        })
                    })
                }
                else
                {
                    DispatchQueue.main.async(execute: {
                        strongSelf.hideLoadingIndicator()
                        hideStatusBarLoadingIndicator()
                    })
                }
            }
        })

        task.resume()
    }
    
    // MARK: - UITableViewDelegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return mtours == nil ? 0 : filteredTours!.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 156
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let identifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let _cell = cell as! HotTourTableViewCell
        let tour = filteredTours![indexPath.row]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: tour.departure)!
        dateFormatter.dateFormat = "dd MMMM"
        let departure = dateFormatter.string(from: date)
        
        _cell.countryLabel.text = tour.country
        _cell.resortLabel.text = tour.resortTour
        _cell.periodLabel.text = "\(departure) на \(tour.period!) дней"
        _cell.priceLabel.text = "\(tour.price!) ₽"
        if let urlstr = tour.preview, let url = URL(string: urlstr)
        {
            _cell.previewImageView.kf.setImage(with: url)
        }
        else
        {
            _cell.previewImageView.image = nil
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HotTourInfoRootVC") as! HotTourInfoRootViewController
        viewController.tour = filteredTours![indexPath.row]
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func didSelectMenuItemIndex(index: Int)
    {
        selectedSegmentIndex = index
        filteredTours = mtours?.tours.filter(
        { (item: HT2Tour) -> Bool in
            
            switch selectedSegmentIndex
            {
                case 0: return true
                case 1: return item.tourType == "Luxury Promo" ? true : false
                case 2: return item.tourType == "Пляжный отдых" ? true : false
                case 3: return item.tourType == "На выходные" ? true : false
                case 4: return item.tourType == "Горные лыжи" ? true : false
                case 5: return item.tourType == "Оздоровление" ? true : false
                default: return false
            }
        })
        tableView.reloadSections(IndexSet(integer: 0), with: UITableViewRowAnimation.automatic)
    }
    
    fileprivate var loadingImageView: UIImageView?
    
    func showLoadingIndicator()
    {
        guard loadingImageView == nil else
        {
            return
        }
        
        loadingImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 160, height: 160))
        loadingImageView!.image = UIImage(named: "loading")
        loadingImageView!.center = self.view.center
        self.view.addSubview(loadingImageView!)
        
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
        
        rotationAnimation.fromValue = 0.0
        rotationAnimation.toValue = Float(M_PI * 2.0)
        rotationAnimation.duration = 2
        rotationAnimation.repeatCount = Float.infinity
        
        loadingImageView!.layer.add(rotationAnimation, forKey: "loadingRotation")
    }
    
    func hideLoadingIndicator()
    {
        guard loadingImageView != nil else
        {
            return
        }
        
        loadingImageView!.layer.removeAnimation(forKey: "loadingRotation")
        loadingImageView!.removeFromSuperview()
        loadingImageView = nil
    }
}
