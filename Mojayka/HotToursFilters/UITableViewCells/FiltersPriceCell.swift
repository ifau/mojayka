//
//  FiltersPriceCell.swift
//  Mojayka
//
//  Created by ifau on 08/05/17.
//  Copyright © 2017 avium. All rights reserved.
//

import UIKit

class FiltersPriceCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descLabel: UILabel!
    
    @IBOutlet var rangeSlider: YSRangeSlider!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
