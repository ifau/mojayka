//
//  HotToursFiltersSelectionHelperViewController.swift
//  Mojayka
//
//  Created by ifau on 29/04/17.
//  Copyright © 2017 avium. All rights reserved.
//

import UIKit

protocol HotToursFiltersSelectionHelperDelegate: class
{
    func numberOfSectionsForSelection(selectionIdentifier: String) -> Int
    func numberOfItemsForSelection(selectionIdentifier: String, section: Int) -> Int
    func titleForItemInSelection(selectionIdentifier: String, section: Int, num: Int) -> String
    func titleForSelection(selectionIdentifier: String, section: Int) -> String?
    
    func didSelectItem(selectionIdentifier: String, section: Int, num: Int)
    func didDeselectItem(selectionIdentifier: String, section: Int, num: Int)
}

class HotToursFiltersSelectionHelperViewController: UIViewController
{
    @IBOutlet var tableView: UITableView!
    var selectionIdentifier: String!
    weak var delegate: HotToursFiltersSelectionHelperDelegate!
    var selectedIndexPaths: [IndexPath]!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if (selectedIndexPaths == nil)
        {
            selectedIndexPaths = []
        }
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    func indexOfIndexPath(indexPath: IndexPath) -> Int?
    {
        for (index, selectedIndexPath) in self.selectedIndexPaths.enumerated()
        {
            if ((selectedIndexPath.row == indexPath.row) && (selectedIndexPath.section == indexPath.section))
            {
                return index
            }
        }
        
        return nil
    }
}

extension HotToursFiltersSelectionHelperViewController: UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return delegate.numberOfSectionsForSelection(selectionIdentifier: selectionIdentifier)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return delegate.numberOfItemsForSelection(selectionIdentifier: selectionIdentifier, section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let title = delegate.titleForItemInSelection(selectionIdentifier: selectionIdentifier, section: indexPath.section, num: indexPath.row)
        
        cell.textLabel?.text = title
        cell.tintColor = Constants.orangeColor()
        
        if self.indexOfIndexPath(indexPath: indexPath) != nil
        {
            cell.accessoryType = .checkmark
        }
        else
        {
            cell.accessoryType = .none
        }
        
        return cell
    }
}

extension HotToursFiltersSelectionHelperViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let index = self.indexOfIndexPath(indexPath: indexPath)
        {
            self.selectedIndexPaths.remove(at: index)
            self.delegate.didDeselectItem(selectionIdentifier: selectionIdentifier, section: indexPath.section, num: indexPath.row)
        }
        else
        {
            self.selectedIndexPaths.append(indexPath)
            self.delegate.didSelectItem(selectionIdentifier: selectionIdentifier, section: indexPath.section, num: indexPath.row)
        }
        self.tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return self.delegate.titleForSelection(selectionIdentifier: selectionIdentifier, section: section)
    }
}
