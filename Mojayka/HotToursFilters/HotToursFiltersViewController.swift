//
//  HotToursFiltersViewController.swift
//  Mojayka
//
//  Created by ifau on 30/04/17.
//  Copyright © 2017 avium. All rights reserved.
//

import UIKit
import STPopup

enum HotToursFiltersIdentifier: String
{
    case Price = "PriceFilter"
    case TourType = "TourTypeFilter"
    case Country = "CountryFilter"
    case Resort = "ResortFilter"
    case Stars = "StarsFilter"
}

class HotToursFiltersViewController: UIViewController
{
    @IBOutlet var tableView: UITableView!
    var priceFormatter: NumberFormatter!
    weak var datasource: HotToursFiltersDataSource!
    weak var toursVC: HotToursViewController!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        priceFormatter = NumberFormatter()
        priceFormatter.locale = Locale(identifier: "ru_RU")
        priceFormatter.maximumFractionDigits = 0
        priceFormatter.numberStyle = NumberFormatter.Style.currency
        datasource.delegate = self
        
        let applyButton = UIBarButtonItem(title: "Применить", style: .done, target: self, action: #selector(HotToursFiltersViewController.applyButtonPressed))
        self.navigationItem.rightBarButtonItem = applyButton
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "ResetCell")
        self.tableView.register(UINib(nibName: "FiltersPriceCell", bundle: nil), forCellReuseIdentifier: "FiltersPriceCell")
        self.tableView.register(UINib(nibName: "FiltersSelectedDescriptionCell", bundle: nil), forCellReuseIdentifier: "FiltersSelectedDescriptionCell")
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 100
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    func applyButtonPressed()
    {
        toursVC.shouldApplyFilters = true
        self.popupController?.dismiss()
    }
    
    func resetButtonPressed()
    {
        datasource.reset()
        toursVC.shouldApplyFilters = false
        self.popupController?.dismiss()
    }
}

extension HotToursFiltersViewController: UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return section == 0 ? 5 : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let identifier = (indexPath.section == 0 && indexPath.row == 0) ? "FiltersPriceCell"
            : (indexPath.section == 1) ? "ResetCell" : "FiltersSelectedDescriptionCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        if (indexPath.section == 0)
        {
            if indexPath.row == 0
            {
                let _cell = cell as! FiltersPriceCell
                _cell.accessoryType = .none
                _cell.titleLabel.text = "Цена"
                
                let min = NSNumber(value: datasource.priceMin)
                let max = NSNumber(value: datasource.priceMax)
                _cell.descLabel.text = "От \(priceFormatter.string(from: min)!) до \(priceFormatter.string(from: max)!)"
                
                _cell.rangeSlider.minimumValue = 0
                _cell.rangeSlider.maximumValue = 500000
                _cell.rangeSlider.minimumSelectedValue = CGFloat(datasource.priceMin)
                _cell.rangeSlider.maximumSelectedValue = CGFloat(datasource.priceMax)
                _cell.rangeSlider.delegate = self
            }
            else if indexPath.row == 1
            {
                let _cell = cell as! FiltersSelectedDescriptionCell
                _cell.titleLabel.text = "Типы туров"
                _cell.titleLabel.textColor = UIColor.black
                if (datasource.availableTypes != nil)
                {
                    _cell.accessoryType = .disclosureIndicator
                    
                    _cell.descLabel.text = datasource.selectedTypesStrings.count == 0 ? "Не выбрано" : datasource.selectedTypesStrings.reduce("", { $0 == "" ? $1 : $0 + ", " + $1 })
                }
                else
                {
                    _cell.accessoryType = .none
                    _cell.descLabel.text = "Загрузка..."
                }
            }
            else if indexPath.row == 2
            {
                let _cell = cell as! FiltersSelectedDescriptionCell
                _cell.titleLabel.text = "Страны"
                _cell.titleLabel.textColor = UIColor.black
                if (datasource.availableCounties != nil)
                {
                    if (datasource.selectedResortsIDs.count > 0)
                    {
                        _cell.titleLabel.textColor = UIColor.lightGray
                        _cell.accessoryType = .none
                        _cell.descLabel.text = "Не выбрано"
                    }
                    else
                    {
                        _cell.titleLabel.textColor = UIColor.black
                        _cell.accessoryType = .disclosureIndicator
                        _cell.descLabel.text = datasource.selectedCountiesStrings.count == 0 ? "Не выбрано" : datasource.selectedCountiesStrings.reduce("", { $0 == "" ? $1 : $0 + ", " + $1 })
                    }
                }
                else
                {
                    _cell.accessoryType = .none
                    _cell.descLabel.text = "Загрузка..."
                }
            }
            else if indexPath.row == 3
            {
                let _cell = cell as! FiltersSelectedDescriptionCell
                _cell.titleLabel.text = "Курорты"
                _cell.titleLabel.textColor = UIColor.black
                if (datasource.availableResorts != nil)
                {
                    if (datasource.selectedCountiesIDs.count > 0)
                    {
                        _cell.titleLabel.textColor = UIColor.lightGray
                        _cell.accessoryType = .none
                        _cell.descLabel.text = "Не выбрано"
                    }
                    else
                    {
                        _cell.titleLabel.textColor = UIColor.black
                        _cell.accessoryType = .disclosureIndicator
                        _cell.descLabel.text = datasource.selectedResortsStrings.count == 0 ? "Не выбрано" : datasource.selectedResortsStrings.reduce("", { $0 == "" ? $1 : $0 + ", " + $1 })
                    }
                }
                else
                {
                    _cell.accessoryType = .none
                    _cell.descLabel.text = "Загрузка..."
                }
            }
            else if indexPath.row == 4
            {
                let _cell = cell as! FiltersSelectedDescriptionCell
                _cell.accessoryType = .disclosureIndicator
                _cell.titleLabel.text = "Звёздность отеля"
                _cell.titleLabel.textColor = UIColor.black
                _cell.descLabel.text = datasource.selectedStartsStrings.count == 0 ? "Не выбрано" : datasource.selectedStartsStrings.reduce("", { $0 == "" ? $1 : $0 + ", " + $1 })
            }
        }
        else if (indexPath.section == 1)
        {
            cell.accessoryType = .none
            cell.selectionStyle = .none
            cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.text = "Сбросить фильтры"
        }
        
        return cell
    }
}

extension HotToursFiltersViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if (indexPath.section == 0)
        {
            var identifier : String?
            var indexPaths : [IndexPath]?
            
            switch indexPath.row
            {
                case 0:
                    return
                case 1:
                    if (datasource.availableTypes != nil)
                    {
                        identifier = HotToursFiltersIdentifier.TourType.rawValue
                        indexPaths = datasource.selectedTypesIndexPaths
                    }
                case 2:
                    if (datasource.availableCounties != nil && datasource.selectedResortsIDs.count == 0)
                    {
                        identifier = HotToursFiltersIdentifier.Country.rawValue
                        indexPaths = datasource.selectedCountiesIndexPaths
                    }
                case 3:
                    if (datasource.availableResorts != nil && datasource.selectedCountiesIDs.count == 0)
                    {
                        identifier = HotToursFiltersIdentifier.Resort.rawValue
                        indexPaths = datasource.selectedResortsIndexPaths
                    }
                case 4:
                    identifier = HotToursFiltersIdentifier.Stars.rawValue
                    indexPaths = datasource.selectedStarsIndexPaths
                default:
                    return
            }
            
            if let selectionIdentifier = identifier, let selectedIndexPaths = indexPaths
            {
                let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HotToursFiltersSelectionHelperVC") as! HotToursFiltersSelectionHelperViewController
                viewController.selectionIdentifier = selectionIdentifier
                viewController.selectedIndexPaths = selectedIndexPaths
                viewController.delegate = self
                
                self.popupController?.push(viewController, animated: true)
            }
        }
        else if (indexPath.section == 1)
        {
            resetButtonPressed()
        }
    }
}

extension HotToursFiltersViewController: YSRangeSLiderDelegate
{
    func rangeSliderDidChange(_ rangeSlider: YSRangeSlider, minimumSelectedValue: CGFloat, maximumSelectedValue: CGFloat)
    {
        datasource.priceMin = Int(minimumSelectedValue)
        datasource.priceMax = Int(maximumSelectedValue)
        
        let priceCellIndexPath = IndexPath(row: 0, section: 0)
        if let cell = tableView.cellForRow(at: priceCellIndexPath) as? FiltersPriceCell
        {        
            let min = NSNumber(value: datasource.priceMin)
            let max = NSNumber(value: datasource.priceMax)
            
            cell.descLabel.text = "От \(priceFormatter.string(from: min)!) до \(priceFormatter.string(from: max)!)"
        }
    }
}

extension HotToursFiltersViewController: HotToursFiltersSelectionHelperDelegate
{
    func numberOfSectionsForSelection(selectionIdentifier: String) -> Int
    {
        switch selectionIdentifier
        {
            case HotToursFiltersIdentifier.Price.rawValue:
                return 1
            case HotToursFiltersIdentifier.TourType.rawValue:
                return 1
            case HotToursFiltersIdentifier.Country.rawValue:
                return 1
            case HotToursFiltersIdentifier.Resort.rawValue:
                
                if let resorts = datasource.availableResorts
                {
                    return resorts.countries.count
                }
                else
                {
                    return 0
                }
                
            case HotToursFiltersIdentifier.Stars.rawValue:
                return 1
            default:
                return 0
        }
    }
    
    func numberOfItemsForSelection(selectionIdentifier: String, section: Int) -> Int
    {
        switch selectionIdentifier
        {
            case HotToursFiltersIdentifier.Price.rawValue:
                return 0
            case HotToursFiltersIdentifier.TourType.rawValue:
                
                if let types = datasource.availableTypes
                {
                    return types.tourtypes.count
                }
                else
                {
                    return 0
                }
                
            case HotToursFiltersIdentifier.Country.rawValue:
                
                if let countries = datasource.availableCounties
                {
                    return countries.countries.count
                }
                else
                {
                    return 0
                }
                
            case HotToursFiltersIdentifier.Resort.rawValue:
                
                if let resorts = datasource.availableResorts
                {
                    return resorts.countries[section].resorts.count
                }
                else
                {
                    return 0
                }
                
            case HotToursFiltersIdentifier.Stars.rawValue:
                return 5
            default:
                return 0
        }
    }
    
    func titleForItemInSelection(selectionIdentifier: String, section: Int, num: Int) -> String
    {
        switch selectionIdentifier
        {
            case HotToursFiltersIdentifier.Price.rawValue:
                return ""
            case HotToursFiltersIdentifier.TourType.rawValue:
                
                if let types = datasource.availableTypes
                {
                    return types.tourtypes[num].title
                }
                else
                {
                    return ""
                }
                
            case HotToursFiltersIdentifier.Country.rawValue:
                
                if let countries = datasource.availableCounties
                {
                    return countries.countries[num].title
                }
                else
                {
                    return ""
                }
                
            case HotToursFiltersIdentifier.Resort.rawValue:
                
                if let resorts = datasource.availableResorts
                {
                    return resorts.countries[section].resorts[num].title
                }
                else
                {
                    return ""
                }
                
            case HotToursFiltersIdentifier.Stars.rawValue:
                return "\(num + 1)*"
            default:
                return ""
        }
    }
    
    func titleForSelection(selectionIdentifier: String, section: Int) -> String?
    {
        switch selectionIdentifier
        {
                
            case HotToursFiltersIdentifier.Resort.rawValue:
                
                if let resorts = datasource.availableResorts
                {
                    return resorts.countries[section].country
                }
                else
                {
                    return nil
                }

            default:
                return nil
        }
    }
    
    func didSelectItem(selectionIdentifier: String, section: Int, num: Int)
    {
        let indexPath = IndexPath(row: num, section: section)
        
        switch selectionIdentifier
        {
            case HotToursFiltersIdentifier.Price.rawValue:
                return
            case HotToursFiltersIdentifier.TourType.rawValue:
                
                let ID = datasource.availableTypes!.tourtypes[num].id!
                let title = datasource.availableTypes!.tourtypes[num].title!
                
                datasource.selectedTypesIDs.append(ID)
                datasource.selectedTypesStrings.append(title)
                datasource.selectedTypesIndexPaths.append(indexPath)
            
            case HotToursFiltersIdentifier.Country.rawValue:
            
                let ID = datasource.availableCounties!.countries[num].id!
                let title = datasource.availableCounties!.countries[num].title!
                
                datasource.selectedCountiesIDs.append(ID)
                datasource.selectedCountiesStrings.append(title)
                datasource.selectedCountiesIndexPaths.append(indexPath)
                
            case HotToursFiltersIdentifier.Resort.rawValue:
            
                let ID = datasource.availableResorts!.countries[section].resorts[num].id!
                let title = datasource.availableResorts!.countries[section].resorts[num].title!
                
                datasource.selectedResortsIDs.append(ID)
                datasource.selectedResortsStrings.append(title)
                datasource.selectedResortsIndexPaths.append(indexPath)
                
            case HotToursFiltersIdentifier.Stars.rawValue:
            
                datasource.selectedStarsIDs.append("\(num + 1)")
                datasource.selectedStartsStrings.append("\(num + 1)*")
                datasource.selectedStarsIndexPaths.append(indexPath)
            
            default:
                return
        }
    }
    
    func didDeselectItem(selectionIdentifier: String, section: Int, num: Int)
    {
        switch selectionIdentifier
        {
            case HotToursFiltersIdentifier.Price.rawValue:
                return
            case HotToursFiltersIdentifier.TourType.rawValue:
                
                let ID = datasource.availableTypes!.tourtypes[num].id!
                let index = datasource.selectedTypesIDs.index(of: ID)!
                
                datasource.selectedTypesIDs.remove(at: index)
                datasource.selectedTypesStrings.remove(at: index)
                datasource.selectedTypesIndexPaths.remove(at: index)
                
            case HotToursFiltersIdentifier.Country.rawValue:
                
                let ID = datasource.availableCounties!.countries[num].id!
                let index = datasource.selectedCountiesIDs.index(of: ID)!
                
                datasource.selectedCountiesIDs.remove(at: index)
                datasource.selectedCountiesStrings.remove(at: index)
                datasource.selectedCountiesIndexPaths.remove(at: index)
                
            case HotToursFiltersIdentifier.Resort.rawValue:
                
                let ID = datasource.availableResorts!.countries[section].resorts[num].id!
                let index = datasource.selectedResortsIDs.index(of: ID)!
                
                datasource.selectedResortsIDs.remove(at: index)
                datasource.selectedResortsStrings.remove(at: index)
                datasource.selectedResortsIndexPaths.remove(at: index)
                
            case HotToursFiltersIdentifier.Stars.rawValue:
                
                let index = datasource.selectedStarsIDs.index(of: "\(num + 1)")!
                
                datasource.selectedStarsIDs.remove(at: index)
                datasource.selectedStartsStrings.remove(at: index)
                datasource.selectedStarsIndexPaths.remove(at: index)
                
            default:
                return
        }
    }
}

extension HotToursFiltersViewController : HotToursFiltersDataSourceDelegate
{
    func filtersDataSourceChanged()
    {
        self.tableView.reloadData()
    }
}
