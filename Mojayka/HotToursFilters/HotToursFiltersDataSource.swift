//
//  HotToursFiltersDataSource.swift
//  Mojayka
//
//  Created by ifau on 07/05/17.
//  Copyright © 2017 avium. All rights reserved.
//

import Foundation

protocol HotToursFiltersDataSourceDelegate: class
{
    func filtersDataSourceChanged()
}

class HotToursFiltersDataSource
{
    var availableTypes: ReferenceTourTypeResponse?
    var availableCounties: ReferenceCountryTourResponse?
    var availableResorts: ReferenceResortTourResponse?
    weak var delegate: HotToursFiltersDataSourceDelegate?
    
    init()
    {
        requestTourTypesFiltersOptions()
        requestCountriesFiltersOptions()
        requestResortsFiltersOptions()
    }
    
    func reset()
    {
        priceMin = 0
        priceMax = 500000
        
        selectedTypesIDs.removeAll()
        selectedTypesStrings.removeAll()
        selectedTypesIndexPaths.removeAll()
        
        selectedCountiesIDs.removeAll()
        selectedCountiesStrings.removeAll()
        selectedCountiesIndexPaths.removeAll()
        
        selectedResortsIDs.removeAll()
        selectedResortsStrings.removeAll()
        selectedResortsIndexPaths.removeAll()
        
        selectedStarsIDs.removeAll()
        selectedStartsStrings.removeAll()
        selectedStarsIndexPaths.removeAll()
        
        delegate?.filtersDataSourceChanged()
    }
    
    func filtersURLParams() -> String
    {
        var params = ""
        params += "&price=\(priceMin),\(priceMax)"
        
        if (selectedTypesIDs.count > 0)
        {
            params += "&tourType="
            params += selectedTypesIDs.reduce("", { $0 == "" ? $1 : $0 + "," + $1 })
        }
        
        if (selectedCountiesIDs.count > 0)
        {
            params += "&countryTour="
            params += selectedCountiesIDs.reduce("", { $0 == "" ? $1 : $0 + "," + $1 })
        }
        
        if (selectedResortsIDs.count > 0)
        {
            params += "&resortTour="
            params += selectedResortsIDs.reduce("", { $0 == "" ? $1 : $0 + "," + $1 })
        }
        
        if (selectedStarsIDs.count > 0)
        {
            params += "&stars="
            params += selectedStarsIDs.reduce("", { $0 == "" ? $1 : $0 + "," + $1 })
        }
        
        return params
    }
    
    var priceMin = 0
    var priceMax = 500000
    
    var selectedTypesIDs : [String] = []
    var selectedTypesStrings : [String] = []
    {
        didSet
        {
            delegate?.filtersDataSourceChanged()
        }
    }
    var selectedTypesIndexPaths : [IndexPath] = []
    
    var selectedCountiesIDs : [String] = []
    var selectedCountiesStrings : [String] = []
    {
        didSet
        {
            delegate?.filtersDataSourceChanged()
        }
    }
    var selectedCountiesIndexPaths : [IndexPath] = []
    
    var selectedResortsIDs : [String] = []
    var selectedResortsStrings : [String] = []
    {
        didSet
        {
            delegate?.filtersDataSourceChanged()
        }
    }
    var selectedResortsIndexPaths : [IndexPath] = []
    
    var selectedStarsIDs : [String] = []
    var selectedStartsStrings : [String] = []
    {
        didSet
        {
            delegate?.filtersDataSourceChanged()
        }
    }
    var selectedStarsIndexPaths : [IndexPath] = []
}

fileprivate extension HotToursFiltersDataSource
{
    func requestTourTypesFiltersOptions()
    {
        let session = URLSession.shared
        let str = "https://mtours.ru/api/reference.json?key=\(apiKey)&reference=tourType"
        let url = URL(string: str)!
        let request = URLRequest(url: url)
        
        showStatusBarLoadingIndicator()
        
        let task = session.dataTask(with: request, completionHandler: { [weak self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            hideStatusBarLoadingIndicator()
            
            if let strongSelf = self
            {
                if error == nil && data != nil
                {
                    DispatchQueue.main.async(execute: {
                        
                        do
                        {
                            let serializedObj = try JSONSerialization.jsonObject(with: data!, options: [])
                            if let jsonDictionary = serializedObj as? NSDictionary
                            {
                                strongSelf.availableTypes = ReferenceTourTypeResponse(fromDictionary: jsonDictionary as! [String : Any])
                                strongSelf.delegate?.filtersDataSourceChanged()
                            }
                        }
                        catch {}
                    })
                }
            }
        })
        
        task.resume()
    }
    
    func requestCountriesFiltersOptions()
    {
        let session = URLSession.shared
        let str = "https://mtours.ru/api/reference.json?key=\(apiKey)&reference=countryTour"
        let url = URL(string: str)!
        let request = URLRequest(url: url)
        
        showStatusBarLoadingIndicator()
        
        let task = session.dataTask(with: request, completionHandler: { [weak self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            hideStatusBarLoadingIndicator()
            
            if let strongSelf = self
            {
                if error == nil && data != nil
                {
                    DispatchQueue.main.async(execute: {
                        
                        do
                        {
                            let serializedObj = try JSONSerialization.jsonObject(with: data!, options: [])
                            if let jsonDictionary = serializedObj as? NSDictionary
                            {
                                strongSelf.availableCounties = ReferenceCountryTourResponse(fromDictionary: jsonDictionary as! [String : Any])
                                strongSelf.delegate?.filtersDataSourceChanged()
                            }
                        }
                        catch { }
                    })
                }
            }
        })
        
        task.resume()
    }
    
    func requestResortsFiltersOptions()
    {
        let session = URLSession.shared
        let str = "https://mtours.ru/api/reference.json?key=\(apiKey)&reference=resortTour"
        let url = URL(string: str)!
        let request = URLRequest(url: url)
        
        showStatusBarLoadingIndicator()
        
        let task = session.dataTask(with: request, completionHandler: { [weak self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            hideStatusBarLoadingIndicator()
            
            if let strongSelf = self
            {
                if error == nil && data != nil
                {
                    DispatchQueue.main.async(execute: {
                        
                        do
                        {
                            let serializedObj = try JSONSerialization.jsonObject(with: data!, options: [])
                            if let jsonDictionary = serializedObj as? NSDictionary
                            {
                                strongSelf.availableResorts = ReferenceResortTourResponse(fromDictionary: jsonDictionary as! [String : Any])
                                strongSelf.delegate?.filtersDataSourceChanged()
                            }
                        }
                        catch { }
                    })
                }
            }
        })
        
        task.resume()
    }
}
