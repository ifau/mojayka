//
//  RequestFeedbackViewController.swift
//  tuiapp
//
//  Created by ifau on 27/11/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class RequestFeedbackViewController: RequestViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction override func submitButtonPressed(_ sender: AnyObject)
    {
        super.submitButtonPressed(sender)
        sendCallBackRequest()
    }
    
    func sendCallBackRequest()
    {
        guard nameTextField.text?.characters.count > 0 && phoneTextField.text?.characters.count > 4 else
        {
            showEmptyFieldsError()
            return
        }
        
        var dict = Dictionary<String,String>()
        
        dict["key"] = apiKey
        dict["Name"] = nameTextField.text
        dict["Phone"] = phoneTextField.text
        dict["EMail"] = nameTextField.text ?? ""
        dict["Comment"] = nameTextField.text ?? ""
        
        sendPostJSONRequest("https://mtours.ru/api/mobile/ordercallback.json", JSONDictionary: dict as Dictionary<String, AnyObject>)
    }
}
