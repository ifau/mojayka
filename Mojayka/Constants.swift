//
//  Constants.swift
//  tuiapp
//
//  Created by ifau on 15/03/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class Constants: NSObject
{
    class func blueColor() -> UIColor
    {
        return UIColor(red: 60/255.0, green: 140/255.0, blue: 197/255.0, alpha: 1)
    }
    
    class func orangeColor() -> UIColor
    {
        return UIColor(red: 1, green: 103/255.0, blue: 30/255.0, alpha: 1)
    }
}
