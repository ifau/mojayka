//
//  SelectDateViewController.swift
//  tuiapp
//
//  Created by ifau on 23/03/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import FSCalendar

protocol SelectDateVCProtocol: class
{
    func selectedDate() -> Date?
    func didSelectDate(_ date: Date)
}

class SelectDateViewController: UIViewController, FSCalendarDelegate
{
    @IBOutlet fileprivate var calendarView: FSCalendar!
    weak var delegate: SelectDateVCProtocol!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if let date = delegate.selectedDate()
        {
            calendarView.select(date)
        }
        
        calendarView.appearance.headerTitleColor = Constants.blueColor()
        calendarView.appearance.weekdayTextColor = Constants.blueColor()
        calendarView.appearance.todayColor = UIColor.lightGray
        calendarView.appearance.selectionColor = Constants.orangeColor()
        calendarView.delegate = self
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date)
    {
        delegate.didSelectDate(date)
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date) -> Bool
    {
        if date.compare(Date()) == ComparisonResult.orderedDescending // date after current date
        {
            return true
        }
        else
        {
            return false
        }
    }
}
