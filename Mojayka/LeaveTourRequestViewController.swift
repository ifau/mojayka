//
//  LeaveTourRequestViewController.swift
//  tuiapp
//
//  Created by ifau on 13/11/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import STPopup

class LeaveTourRequestViewController: UIViewController
{
    var date: Date?
    var moneyMin: CGFloat = 10000
    var moneyMax: CGFloat = 50000
    
    @IBOutlet var contryTextField: UITextField!
    @IBOutlet var dateButton: UIButton!
    @IBOutlet var daysfromTextField: UITextField!
    @IBOutlet var daystoTextField: UITextField!
    @IBOutlet var moneyLabel: UILabel!
    @IBOutlet var moneySlider: YSRangeSlider!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        initialize()
    }
    
    func initialize()
    {
        contryTextField.layer.borderColor = Constants.blueColor().cgColor
        contryTextField.layer.borderWidth = 1
        contryTextField.backgroundColor = UIColor.clear
        contryTextField.layer.cornerRadius = 2
        contryTextField.delegate = self
        
        dateButton.layer.borderColor = Constants.blueColor().cgColor
        dateButton.layer.borderWidth = 1
        dateButton.backgroundColor = UIColor.clear
        dateButton.layer.cornerRadius = 2
        dateButton.addTarget(self, action: #selector(LeaveTourRequestViewController.presentDatePicker), for: .touchUpInside)
        
        daysfromTextField.layer.borderColor = Constants.blueColor().cgColor
        daysfromTextField.layer.borderWidth = 1
        daysfromTextField.backgroundColor = UIColor.clear
        daysfromTextField.layer.cornerRadius = 2
        daysfromTextField.delegate = self
        
        daystoTextField.layer.borderColor = Constants.blueColor().cgColor
        daystoTextField.layer.borderWidth = 1
        daystoTextField.backgroundColor = UIColor.clear
        daystoTextField.layer.cornerRadius = 2
        daystoTextField.delegate = self
        
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LeaveTourRequestViewController.hideKeyboard))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
        
        moneySlider.minimumValue = 10000
        moneySlider.maximumValue = 200000
        moneySlider.minimumSelectedValue = moneyMin
        moneySlider.maximumSelectedValue = moneyMax
        moneySlider.delegate = self
        
        updateMoneyLabel()
    }
    
    func updateMoneyLabel()
    {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.maximumFractionDigits = 0
        formatter.numberStyle = NumberFormatter.Style.currency
        
        let min = NSNumber(value: Int(moneyMin) as Int)
        let max = NSNumber(value: Int(moneyMax) as Int)
        
        var text = "От \(formatter.string(from: min)!) до \(formatter.string(from: max)!)"
        if moneyMax == moneySlider.maximumValue
        {
            text += " и более"
        }
        moneyLabel.text = text
    }
    
    func updateDateButton()
    {
        if let _date = date
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMMM yyyy"
            let text = dateFormatter.string(from: _date)
            dateButton.setTitle(text, for: UIControlState())
        }
    }
    
    func hideKeyboard()
    {
        view.endEditing(true)
    }
    
//    override var disablesAutomaticKeyboardDismissal : Bool
//    {
//        return false
//    }
    
    func presentDatePicker()
    {
        hideKeyboard()
        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectDateVC") as! SelectDateViewController
        viewController.delegate = self
        
        let popupController = STPopupController(rootViewController: viewController)
        popupController.style = STPopupStyle.bottomSheet
        popupController.present(in: self)
    }
    
    @IBAction func requestButtonPressed(_ sender: AnyObject)
    {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RequestPersonalTourVC") as! RequestPersonalTourViewController
        
        viewController.country = contryTextField.text
        viewController.date = date
        viewController.daysfrom = daysfromTextField.text
        viewController.daysto = daystoTextField.text
        viewController.pricefrom = "\(Int(moneyMin))"
        viewController.priceto = "\(Int(moneyMax)))"
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension LeaveTourRequestViewController: SelectDateVCProtocol
{
    func selectedDate() -> Date?
    {
        return date
    }
    
    func didSelectDate(_ date: Date)
    {
        self.date = date
        updateDateButton()
    }
}

extension LeaveTourRequestViewController: YSRangeSLiderDelegate
{
    func rangeSliderDidChange(_ rangeSlider: YSRangeSlider, minimumSelectedValue: CGFloat, maximumSelectedValue: CGFloat)
    {
        moneyMin = minimumSelectedValue
        moneyMax = maximumSelectedValue
        updateMoneyLabel()
    }
}

extension LeaveTourRequestViewController: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField === contryTextField
        {
            daysfromTextField.becomeFirstResponder()
        }
        else if textField === daysfromTextField
        {
            daystoTextField.becomeFirstResponder()
        }
        else if textField === daystoTextField
        {
            textField.resignFirstResponder()
        }
        return false
    }
}
