//
//  HotTourInfoTabGallery.swift
//  tuiapp
//
//  Created by ifau on 05/03/17.
//  Copyright © 2017 tabus. All rights reserved.
//

import UIKit
import INSPhotoGallery

class HotTourInfoTabGallery: UIViewController
{
    @IBOutlet weak var collectionView: UICollectionView!
    fileprivate var photos: [INSPhotoViewable] = []
    var hotelImages: [HAHotelImage] = []
    {
        didSet
        {
            for hotelImage in hotelImages
            {
                if let url = hotelImage.imageUrl
                {
                    let photo = INSPhoto(imageURL: URL(string: url), thumbnailImageURL: URL(string: url))
                    photos.append(photo)
                    
                    if (collectionView != nil)
                    {
                        collectionView.reloadData()
                    }
                }
            }
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(ThumbCollectionViewCell.self, forCellWithReuseIdentifier: "ThumbCollectionViewCell")
        collectionView.reloadData()
    }
}

class ThumbCollectionViewCell: UICollectionViewCell
{
    var imageView: UIImageView!
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        self.contentView.addSubview(imageView)

        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[imageView]-0-|", options: .directionLeftToRight, metrics: nil, views: ["imageView" : imageView]))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[imageView]-0-|", options: .directionLeftToRight, metrics: nil, views: ["imageView" : imageView]))
    }
    
    convenience init()
    {
        self.init(frame: CGRect.zero)
    }
    
    required convenience init(coder aDecoder: NSCoder)
    {
        self.init(frame: CGRect.zero)
    }
    
    func populateWithPhoto(_ photo: INSPhotoViewable)
    {
        photo.loadThumbnailImageWithCompletionHandler { [weak photo] (image, error) in
            if let image = image
            {
                if let photo = photo as? INSPhoto
                {
                    photo.thumbnailImage = image
                }
                self.imageView.image = image
            }
        }
    }
}

extension HotTourInfoTabGallery: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThumbCollectionViewCell", for: indexPath) as! ThumbCollectionViewCell
        cell.populateWithPhoto(photos[(indexPath as NSIndexPath).row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let cell = collectionView.cellForItem(at: indexPath) as! ThumbCollectionViewCell
        let currentPhoto = photos[(indexPath as NSIndexPath).row]
        let galleryPreview = INSPhotosViewController(photos: photos, initialPhoto: currentPhoto, referenceView: cell)
        
        galleryPreview.referenceViewForPhotoWhenDismissingHandler = { [weak self] photo in
            if let index = self?.photos.index(where: {$0 === photo})
            {
                let indexPath = IndexPath(item: index, section: 0)
                return collectionView.cellForItem(at: indexPath) as? ThumbCollectionViewCell
            }
            return nil
        }
        present(galleryPreview, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let size = collectionView.frame.size.width/3.0 - 10
        return CGSize(width: size, height: size)
    }
}
