//
//  HotTourInfoTabMain.swift
//  tuiapp
//
//  Created by ifau on 25/03/17.
//  Copyright © 2017 tabus. All rights reserved.
//

import UIKit

class HotTourInfoTabMain: UIViewController
{
    var tour: HT2Tour!
    
    @IBOutlet var infoLabel: UILabel!
    @IBOutlet var sendButton: UIButton!
    @IBOutlet var pictureImageView: UIImageView!
    @IBOutlet var weatherImageView: UIImageView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        sendButton.setTitleColor(UIColor.white, for: UIControlState())
        sendButton.backgroundColor = Constants.orangeColor()
        
        infoLabel.attributedText = generateDescription()
        if let urlstr = tour.preview, let url = URL(string: urlstr)
        {
            pictureImageView.kf.setImage(with: url)
        }
        if let weatherurlstr = tour.infoWeather, let weatherurl = URL(string: weatherurlstr)
        {
            weatherImageView.kf.setImage(with: weatherurl)
        }
    }
    
    func generateDescription() -> NSAttributedString
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: tour.departure)!
        dateFormatter.dateFormat = "dd MMMM yyyy"
        
        let line1 = "\(tour.country!) за \(tour.price!) ₽"
        let line2 = "Курорт: \(tour.resortTour!)"
        let line3 = "Дней: \(tour.period!)"
        let line4 = "Отель: \(tour.hotel!) (\(tour.hotelStar!)*)"
        let line5 = "В стоимость тура входит: \(tour.contents!)"
        let line6 = "Дата вылета: \(dateFormatter.string(from: date))"
        let line7 = tour.additionally!
        let line8 = "Цена одноместного размещения: \(tour.priceOne!) ₽";
        
        let descriptionText = "\(line1)\n\n\(line2)\n\n\(line3)\n\n\(line4)\n\n\(line5)\n\n\(line6)\n\n\(line7)\n\n\(line8)"
        
        let attributedDescription = NSMutableAttributedString(string: descriptionText)
        attributedDescription.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue", size: 12)!, range: NSRange(location: 0, length: descriptionText.characters.count))
        attributedDescription.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location: 0, length: descriptionText.characters.count))
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
        attributedDescription.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: (descriptionText as NSString).range(of: line1))
        attributedDescription.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue-Medium", size: 14)!, range: (descriptionText as NSString).range(of: line1))
        
        attributedDescription.addAttribute(NSForegroundColorAttributeName, value: Constants.orangeColor(), range: (descriptionText as NSString).range(of: line1))
        attributedDescription.addAttribute(NSForegroundColorAttributeName, value: Constants.orangeColor(), range: (descriptionText as NSString).range(of: "Курорт:"))
        attributedDescription.addAttribute(NSForegroundColorAttributeName, value: Constants.orangeColor(), range: (descriptionText as NSString).range(of: "Дней:"))
        attributedDescription.addAttribute(NSForegroundColorAttributeName, value: Constants.orangeColor(), range: (descriptionText as NSString).range(of: "Отель:"))
        attributedDescription.addAttribute(NSForegroundColorAttributeName, value: Constants.orangeColor(), range: (descriptionText as NSString).range(of: "В стоимость тура входит:"))
        attributedDescription.addAttribute(NSForegroundColorAttributeName, value: Constants.orangeColor(), range: (descriptionText as NSString).range(of: "Дата вылета:"))
        
        return attributedDescription
    }
    
    @IBAction func sendButtonPressed(_ sender: AnyObject)
    {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RequestHotTourVC") as! RequestHotTourViewController
        viewController.tour = tour
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
