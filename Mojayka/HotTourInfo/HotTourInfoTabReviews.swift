//
//  HotTourInfoTabReviews.swift
//  tuiapp
//
//  Created by ifau on 26/03/17.
//  Copyright © 2017 tabus. All rights reserved.
//

import UIKit

class HotTourInfoTabReviews: UIViewController
{
    var reviews : [HRTophotel] = []
    {
        didSet
        {
            if tableView != nil
            {
                tableView.reloadData()
            }
        }
    }
    
    @IBOutlet var tableView: UITableView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension HotTourInfoTabReviews: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return reviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! reviewTableCell
        let review = reviews[indexPath.row]
        var content = ""
        
        if (review.title != nil)
        {
            content += review.title!
        }
        if (review.content != nil && review.title != nil)
        {
            content += "\n\n\(review.content!)"
        }
        else if (review.content != nil)
        {
            content += review.content!
        }
        
        cell.authorLabel.text = review.author != nil ? review.author! : ""
        cell.ratingLabel.text = review.rating != nil ? "\(review.rating!)" : ""
        cell.contentLabel.text = content.withoutMultiWhitespaces
        
        return cell
    }
}

class reviewTableCell: UITableViewCell
{
    @IBOutlet var authorLabel: UILabel!
    @IBOutlet var ratingLabel: UILabel!
    @IBOutlet var contentLabel: UILabel!
}

fileprivate extension String
{
    var withoutMultiWhitespaces: String
    {
        let components = self.components(separatedBy: .whitespaces)
        return components.filter { !$0.isEmpty }.joined(separator: " ")
    }
}
