//
//  HotTourInfoRootViewController.swift
//  tuiapp
//
//  Created by ifau on 25/03/17.
//  Copyright © 2017 tabus. All rights reserved.
//

import UIKit
import PageMenu

class HotTourInfoRootViewController: UIViewController
{
    var tour: HT2Tour!
    var pageMenu: CAPSPageMenu?
    var additionally: HotelAdditionallyResponse?
    {
        didSet
        {
            if let info = additionally, let h = info.hotel, let _ = h.hotelImages
            {
                galleryVC.hotelImages = info.hotel.hotelImages
            }
            
            if let info = additionally, let h = info.hotel, let _ = h.hotelProperties
            {
                serviceVC.hotelProperties = info.hotel.hotelProperties
            }
        }
    }
    var reviews: HotelReviewsResponse?
    {
        didSet
        {
            if let rev = reviews, let revarray = rev.tophotel
            {
                reviewsVC.reviews = revarray
            }
        }
    }
    
    var galleryVC: HotTourInfoTabGallery!
    var serviceVC: HotTourInfoTabAdditionalService!
    var reviewsVC: HotTourInfoTabReviews!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        initializePages()
        requestAdditionaly()
        requestReviews()
    }
    
    func initializePages()
    {
        var vcs: [UIViewController] = []
        print("\(tour.id!)")
        let infoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HotTourInfoVC") as! HotTourInfoTabMain
        infoVC.title = "Описание"
        infoVC.tour = tour
        vcs.append(infoVC)
        
        galleryVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HotTourInfoTabGalleryVC") as! HotTourInfoTabGallery
        galleryVC.title = "Галерея"
        vcs.append(galleryVC)
        
        serviceVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HotTourInfoTabServiceVC") as! HotTourInfoTabAdditionalService
        serviceVC.title = "Услуги"
        vcs.append(serviceVC)
        
        reviewsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HotTourInfoTabReviewsVC") as! HotTourInfoTabReviews
        reviewsVC.title = "Отзывы"
        vcs.append(reviewsVC)
        
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(Constants.blueColor()),
            .selectionIndicatorColor(Constants.orangeColor()),
            .bottomMenuHairlineColor(Constants.orangeColor()),
            .selectedMenuItemLabelColor(Constants.orangeColor()),
            .unselectedMenuItemLabelColor(UIColor.white),
            .menuHeight(40.0),
            .centerMenuItems(true)
        ]
        
        pageMenu = CAPSPageMenu(viewControllers: vcs, frame: CGRect(x: 0.0, y: self.topLayoutGuide.length, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        pageMenu!.view.backgroundColor = UIColor.clear
        
        self.addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParentViewController: self)
    }
    
    func generateDescription() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: tour.departure)!
        dateFormatter.dateFormat = "dd MMMM yyyy"
        
        let line1 = "\(tour.country!) за \(tour.price!) ₽"
        let line2 = "Курорт: \(tour.resortTour!)"
        let line3 = "Дней: \(tour.period!)"
        let line4 = "Отель: \(tour.hotel!) (\(tour.hotelStar)*)"
        let line5 = "В стоимость тура входит: \(tour.contents!)"
        let line6 = "Дата вылета: \(dateFormatter.string(from: date))"
        let line7 = tour.additionally!
        let line8 = "Цена одноместного размещения: \(tour.priceOne!) ₽";
        
        let descriptionText = "\(line1)\n\n\(line2)\n\n\(line3)\n\n\(line4)\n\n\(line5)\n\n\(line6)\n\n\(line7)\n\n\(line8)"
        
        return descriptionText
    }
    
    @IBAction func shareTourButtonPressed(_ sender: AnyObject)
    {
        let text = "Смотри что я нашел! WoW тур в \(generateDescription())"
        let activityController = UIActivityViewController(activityItems: [text], applicationActivities: nil)
        self.present(activityController, animated: true, completion: nil)
    }
    
    var requestAdditionalyTask : URLSessionDataTask?
    var requestReviewsTask : URLSessionDataTask?
    
    deinit
    {
        if (requestAdditionalyTask != nil)
        {
            requestAdditionalyTask!.cancel()
        }
        
        if (requestReviewsTask != nil)
        {
            requestAdditionalyTask!.cancel()
        }
    }
    
    func requestAdditionaly()
    {
        let session = URLSession.shared
        let str = "https://mtours.ru/api/info-tour/hotel-additionally.json?tour_id=\(tour.id!)&key=\(apiKey)"
        let url = URL(string: str)!
        let request = URLRequest(url: url)
        
        showStatusBarLoadingIndicator()
        requestAdditionalyTask = session.dataTask(with: request, completionHandler: { [weak self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            hideStatusBarLoadingIndicator()
            if let strongSelf = self
            {
                if error == nil && data != nil
                {
                    DispatchQueue.main.async(execute: {
                        
                        do
                        {
                            let serializedObj = try JSONSerialization.jsonObject(with: data!, options: [])
                            if let jsonDictionary = serializedObj as? NSDictionary
                            {
                            strongSelf.additionally = HotelAdditionallyResponse(fromDictionary: jsonDictionary as! [String : Any])
                            }
                        }
                        catch {}
                    })
                }
                else
                {

                }
            }
        })
        requestAdditionalyTask!.resume()
    }
    
    func requestReviews()
    {
        let session = URLSession.shared
        let str = "https://mtours.ru/api/info-tour/hotel-reviews.json?tour_id=\(tour.id!)&key=\(apiKey)"
        let url = URL(string: str)!
        let request = URLRequest(url: url)
        
        showStatusBarLoadingIndicator()
        requestReviewsTask = session.dataTask(with: request, completionHandler: { [weak self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            hideStatusBarLoadingIndicator()
            if let strongSelf = self
            {
                if error == nil && data != nil
                {
                    DispatchQueue.main.async(execute: {
                        
                        do
                        {
                            let serializedObj = try JSONSerialization.jsonObject(with: data!, options: [])
                            if let jsonDictionary = serializedObj as? NSDictionary
                            {
                            strongSelf.reviews = HotelReviewsResponse(fromDictionary: jsonDictionary as! [String : Any])
                            }
                        }
                        catch { }
                    })
                }
                else
                {
                    
                }
            }
        })
        
        requestReviewsTask!.resume()
    }
}
