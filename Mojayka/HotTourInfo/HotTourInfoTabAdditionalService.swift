//
//  HotTourInfoTabAdditionalService.swift
//  tuiapp
//
//  Created by ifau on 26/03/17.
//  Copyright © 2017 tabus. All rights reserved.
//

import UIKit

class HotTourInfoTabAdditionalService: UIViewController
{
    var hotelProperties : [HAHotelProperty] = []
    {
        didSet
        {
            generateDescription()
        }
    }
    
    @IBOutlet var textView: UITextView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        generateDescription()
    }
    
    func generateDescription()
    {
        guard textView != nil else { return }
        
        let hotelDescription = NSMutableAttributedString(string: "")
        
        let groupedProperties = hotelProperties.group { $0.propertyGroup }
        for (category, categoryProperties) in groupedProperties
        {
            let categoryText = "\(category)\n\n"
            let attrCategoryText = NSMutableAttributedString(string: categoryText)
            attrCategoryText.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue-Medium", size: 14)!, range: NSRange(location: 0, length: categoryText.characters.count))
            attrCategoryText.addAttribute(NSForegroundColorAttributeName, value: Constants.orangeColor(), range: NSRange(location: 0, length: categoryText.characters.count))
            hotelDescription.append(attrCategoryText)
            
            for property in categoryProperties
            {
                var propertyText : String? = nil
                
                if property.dataType == "bool" && property.boolValue
                {
                    propertyText = "\(property.descriptionField!)\n\n"
                }
                
                if property.dataType == "payFreeSpecial" && property.boolValue
                {
                    propertyText = "\(property.descriptionField!)\n\n"
                }
                
                if property.dataType == "int"
                {
                    propertyText = "\(property.descriptionField!): \(property.intValue!)\n\n"
                }
                
                if property.dataType == "float"
                {
                    propertyText = "\(property.descriptionField!): \(property.floatValue!)\n\n"
                }
                
                if property.dataType == "string"
                {
                    propertyText = "\(property.descriptionField!): \(property.stringValue!)\n\n"
                }
                
                if propertyText != nil
                {
                    let attrPropertyText = NSMutableAttributedString(string: propertyText!)
                    attrPropertyText.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue", size: 12)!, range: NSRange(location: 0, length: propertyText!.characters.count))
                    attrPropertyText.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location: 0, length: propertyText!.characters.count))
                    hotelDescription.append(attrPropertyText)
                }
            }
        }
        
        textView.attributedText = hotelDescription
    }
}

fileprivate extension Sequence
{
    func group<U: Hashable>(by key: (Iterator.Element) -> U) -> [U:[Iterator.Element]]
    {
        var categories: [U: [Iterator.Element]] = [:]
        for element in self
        {
            let key = key(element)
            if case nil = categories[key]?.append(element)
            {
                categories[key] = [element]
            }
        }
        return categories
    }
}
