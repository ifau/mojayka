//
//  ReferenceResortTourResponse.swift
//  Mojayka
//
//  Created by ifau on 29/04/17.
//  Copyright © 2017 avium. All rights reserved.
//

import Foundation

class ReferenceResortTourResponse
{
    var countries: [RRConutry]!
    
    init(fromDictionary dictionary: [String:Any])
    {
        countries = []
        if let referenceDictionary = dictionary["reference"] as? [String:Any]
        {
            if let resortTourDictionary = referenceDictionary["resortTour"] as? [String:Any]
            {
                if let treeDictionary = resortTourDictionary["TREE"] as? [String:Any]
                {
                    let countriesKeys = treeDictionary.keys
                    
                    for countryString in countriesKeys
                    {
                        let country = RRConutry()
                        country.country = countryString
                        country.resorts = []
                        
                        let resortDictionary = treeDictionary[countryString] as! [String:Any]
                        
                        let resortIDs = resortDictionary.keys
                        
                        for resortID in resortIDs
                        {
                            let resort = RRResort()
                            resort.id = resortID
                            resort.title = resortDictionary[resortID] as! String
                            
                            country.resorts.append(resort)
                        }
                        
                        countries.append(country)
                    }
                }
            }
        }
    }
}

class RRConutry
{
    var country: String!
    var resorts: [RRResort]!
}

class RRResort
{
    var title: String!
    var id: String!
}
