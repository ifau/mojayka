//
//  HotelReviewsResponse.swift
//  tuiapp
//
//  Created by ifau on 05/03/17.
//  Copyright © 2017 tabus. All rights reserved.
//

import Foundation

class HotelReviewsResponse
{
    var tophotel : [HRTophotel]!

    init(fromDictionary dictionary: [String:Any]){
        tophotel = [HRTophotel]()
        if let tophotelArray = dictionary["tophotel"] as? [[String:Any]]{
            for dic in tophotelArray{
                let value = HRTophotel(fromDictionary: dic)
                tophotel.append(value)
            }
        }
    }
}

class HRTophotel
{
    var author : String!
    var content : String!
    var rating : Int!
    var title : String!

    init(fromDictionary dictionary: [String:Any]){
        author = dictionary["author"] as? String
        content = dictionary["content"] as? String
        rating = dictionary["rating"] as? Int
        title = dictionary["title"] as? String
    }
}
