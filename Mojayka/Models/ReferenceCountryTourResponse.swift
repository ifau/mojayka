//
//  ReferenceCountryTourResponse.swift
//  Mojayka
//
//  Created by ifau on 30/04/17.
//  Copyright © 2017 avium. All rights reserved.
//

import Foundation

class ReferenceCountryTourResponse
{
    var countries: [RCCountry]!
    
    init(fromDictionary dictionary: [String:Any])
    {
        countries = []
        if let referenceDictionary = dictionary["reference"] as? [String:Any]
        {
            if let countryTourDictionary = referenceDictionary["countryTour"] as? [String:Any]
            {
                let countryTourIDs = countryTourDictionary.keys
                
                for countryTourID in countryTourIDs
                {
                    let country = RCCountry()
                    country.id = countryTourID
                    country.title = countryTourDictionary[countryTourID] as! String
                    
                    countries.append(country)
                }
            }
        }
    }
}

class RCCountry
{
    var title: String!
    var id: String!
}
