//
//  HotTours2Response.swift
//  tuiapp
//
//  Created by ifau on 05/03/17.
//  Copyright © 2017 tabus. All rights reserved.
//

import Foundation

class HotTours2Response
{
    var count : Int!
    var tours : [HT2Tour]!

    init(fromDictionary dictionary: [String:Any]){
        count = dictionary["count"] as? Int
        tours = [HT2Tour]()
        if let toursArray = dictionary["tours"] as? [[String:Any]]{
            for dic in toursArray{
                let value = HT2Tour(fromDictionary: dic)
                tours.append(value)
            }
        }
    }
}

class HT2Tour
{
    var additionally : String!
    var contents : String!
    var country : String!
    var createdon : String!
    var departure : String!
    var hotel : String!
    var hotelStar : String!
    var id : String!
    var infoWeather : String!
    var operator_ : String!
    var period : String!
    var price : String!
    var priceOne : String!
    var resortTour : String! //HT2ResortTour!
    var tourType : String! //HT2TourType!
    
    var preview : String!
    
    init(fromDictionary dictionary: [String:Any]){
        additionally = dictionary["additionally"] as? String
        contents = dictionary["contents"] as? String
        country = dictionary["country"] as? String
        createdon = dictionary["createdon"] as? String
        departure = dictionary["departure"] as? String
        hotel = dictionary["hotel"] as? String
        hotelStar = dictionary["hotel_star"] as? String
        id = dictionary["id"] as? String
        operator_ = dictionary["operator"] as? String
        period = dictionary["period"] as? String
        price = dictionary["price"] as? String
        priceOne = dictionary["price_one"] as? String
        if let resortTourData = dictionary["resortTour"] as? [String:Any]
        {
//            resortTour = HT2ResortTour(fromDictionary: resortTourData)
            resortTour = resortTourData["pagetitle"] as? String
            if let pictureArray = resortTourData["picture"] as? [[String:Any]], pictureArray.count > 0
            {
                if let po = pictureArray[0] as? NSDictionary, let puri = po["imageBig"] as? String
                {
                    preview = "http://mtours.ru/\(puri)"
                }
            }
            
        }
        if let tourTypeData = dictionary["tourType"] as? [String:Any]
        {
//            tourType = HT2TourType(fromDictionary: tourTypeData)
            tourType = tourTypeData["name"] as? String
        }
        if let weatherString = dictionary["info_weather"] as? String
        {
            infoWeather = "http:\(weatherString)"
        }
    }
}

//class HT2TourType
//{
//    var id : String!
//    var name : String!
//
//    init(fromDictionary dictionary: [String:Any]){
//        id = dictionary["id"] as? String
//        name = dictionary["name"] as? String
//    }
//}

//class HT2ResortTour
//{
//    var pagetitle : String!
//    var picture : [HT2Picture]!
//
//    init(fromDictionary dictionary: [String:Any]){
//        pagetitle = dictionary["pagetitle"] as? String
//        picture = [HT2Picture]()
//        if let pictureArray = dictionary["picture"] as? [[String:Any]]{
//            for dic in pictureArray{
//                let value = HT2Picture(fromDictionary: dic)
//                picture.append(value)
//            }
//        }
//    }
//}
//
//class HT2Picture
//{
//    var mIGXId : String!
//    var image : String!
//    var imageBig : String!
//    var imgDescription : String!
//    var name : String!
//
//    init(fromDictionary dictionary: [String:Any]){
//        mIGXId = dictionary["MIGX_id"] as? String
//        image = dictionary["image"] as? String
//        imageBig = dictionary["imageBig"] as? String
//        imgDescription = dictionary["img_description"] as? String
//        name = dictionary["name"] as? String
//    }
//}
