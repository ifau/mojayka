//
//  ReferenceTourTypeResponse.swift
//  Mojayka
//
//  Created by ifau on 29/04/17.
//  Copyright © 2017 avium. All rights reserved.
//

import Foundation

class ReferenceTourTypeResponse
{
    var tourtypes: [RTTourType]!
    
    init(fromDictionary dictionary: [String:Any])
    {
        tourtypes = []
        if let referenceDictionary = dictionary["reference"] as? [String:Any]
        {
            if let tourTypeDictionary = referenceDictionary["tourType"] as? [String:Any]
            {
                let tourTypeIDs = tourTypeDictionary.keys
                
                for tourTypeID in tourTypeIDs
                {
                    let tourtype = RTTourType()
                    tourtype.id = tourTypeID
                    tourtype.title = tourTypeDictionary[tourTypeID] as! String
                    
                    tourtypes.append(tourtype)
                }
            }
        }
    }
}

class RTTourType
{
    var title: String!
    var id: String!
}
