//
//  HotelAdditionallyResponse.swift
//  tuiapp
//
//  Created by ifau on 05/03/17.
//  Copyright © 2017 tabus. All rights reserved.
//

import Foundation

class HotelAdditionallyResponse
{
    var hotel : HAHotel!
    var video : [String]!

    init(fromDictionary dictionary: [String:Any]){
        if let hotelData = dictionary["hotel"] as? [String:Any]{
            hotel = HAHotel(fromDictionary: hotelData)
        }
        video = dictionary["video"] as? [String]
    }
}

class HAHotel
{
    var id : Int!
    var plainName : String!
    var reviews : [AnyObject]!
    var beachDistance : Int!
    var countryId : Int!
    var countryName : String!
    var countryTranslit : String!
    var foodRating : Int!
    var hotelImages : [HAHotelImage]!
    var hotelProperties : [HAHotelProperty]!
    var hotelPropertyExtendeds : [AnyObject]!
    var hotelTranslit : String!
    var mainImage : String!
    var mainImageSizeM : String!
    var mainRating : Float!
    var name : String!
    var regionId : Int!
    var regionName : AnyObject!
    var regionTranslit : AnyObject!
    var resortId : Int!
    var resortName : String!
    var resortTranslit : String!
    var roomRating : Int!
    var serviceRating : Int!
    var starId : Int!
    var starName : String!
    var stars : Int!

    init(fromDictionary dictionary: [String:Any]){
        id = dictionary["Id"] as? Int
        plainName = dictionary["PlainName"] as? String
        reviews = dictionary["Reviews"] as? [AnyObject]
        beachDistance = dictionary["beachDistance"] as? Int
        countryId = dictionary["countryId"] as? Int
        countryName = dictionary["countryName"] as? String
        countryTranslit = dictionary["countryTranslit"] as? String
        foodRating = dictionary["foodRating"] as? Int
        hotelImages = [HAHotelImage]()
        if let hotelImagesArray = dictionary["hotelImages"] as? [[String:Any]]{
            for dic in hotelImagesArray{
                let value = HAHotelImage(fromDictionary: dic)
                hotelImages.append(value)
            }
        }
        hotelProperties = [HAHotelProperty]()
        if let hotelPropertiesArray = dictionary["hotelProperties"] as? [[String:Any]]{
            for dic in hotelPropertiesArray{
                let value = HAHotelProperty(fromDictionary: dic)
                hotelProperties.append(value)
            }
        }
        hotelPropertyExtendeds = dictionary["hotelPropertyExtendeds"] as? [AnyObject]
        hotelTranslit = dictionary["hotelTranslit"] as? String
        mainImage = dictionary["mainImage"] as? String
        mainImageSizeM = dictionary["mainImageSizeM"] as? String
        mainRating = dictionary["mainRating"] as? Float
        name = dictionary["name"] as? String
        regionId = dictionary["regionId"] as? Int
        regionName = dictionary["regionName"] as AnyObject
        regionTranslit = dictionary["regionTranslit"] as AnyObject
        resortId = dictionary["resortId"] as? Int
        resortName = dictionary["resortName"] as? String
        resortTranslit = dictionary["resortTranslit"] as? String
        roomRating = dictionary["roomRating"] as? Int
        serviceRating = dictionary["serviceRating"] as? Int
        starId = dictionary["starId"] as? Int
        starName = dictionary["starName"] as? String
        stars = dictionary["stars"] as? Int
    }
}

class HAHotelProperty
{
    var boolValue : Bool!
    var code : String!
    var dataType : String!
    var dateValue : String!
    var descriptionField : String!
    var floatValue : Float!
    var id : String!
    var intValue : Int!
    var name : String!
    var payFreeSpecialEnum : Int!
    var payFreeSpecialValue : Int!
    var propertyGroup : String!
    var specialCondition : AnyObject!
    var stringValue : String!
    var textValue : AnyObject!

    init(fromDictionary dictionary: [String:Any]){
        boolValue = dictionary["boolValue"] as? Bool
        code = dictionary["code"] as? String
        dataType = dictionary["dataType"] as? String
        dateValue = dictionary["dateValue"] as? String
        descriptionField = dictionary["description"] as? String
        floatValue = dictionary["floatValue"] as? Float
        id = dictionary["id"] as? String
        intValue = dictionary["intValue"] as? Int
        name = dictionary["name"] as? String
        payFreeSpecialEnum = dictionary["payFreeSpecialEnum"] as? Int
        payFreeSpecialValue = dictionary["payFreeSpecialValue"] as? Int
        propertyGroup = dictionary["propertyGroup"] as? String
        specialCondition = dictionary["specialCondition"] as AnyObject
        stringValue = dictionary["stringValue"] as? String
        textValue = dictionary["textValue"] as AnyObject
    }
}

class HAHotelImage
{
    var height : Int!
    var imageOrder : Int!
    var imageType : Int!
    var imageUrl : String!
    var isMainImage : Bool!
    var width : Int!

    init(fromDictionary dictionary: [String:Any]){
        height = dictionary["height"] as? Int
        imageOrder = dictionary["imageOrder"] as? Int
        imageType = dictionary["imageType"] as? Int
        imageUrl = dictionary["imageUrl"] as? String
        isMainImage = dictionary["isMainImage"] as? Bool
        width = dictionary["width"] as? Int
    }
}
