//
//  RequestPersonalTourViewController.swift
//  tuiapp
//
//  Created by ifau on 27/11/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class RequestPersonalTourViewController: RequestViewController
{
    var country: String?
    var date: Date?
    var daysfrom: String?
    var daysto: String?
    var pricefrom: String?
    var priceto: String?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction override func submitButtonPressed(_ sender: AnyObject)
    {
        super.submitButtonPressed(sender)
        sendPersonalTourRequest()
    }
    
    func sendPersonalTourRequest()
    {
        guard nameTextField.text?.characters.count > 0 && phoneTextField.text?.characters.count > 4 else
        {
            showEmptyFieldsError()
            return
        }
        
        var dict = Dictionary<String,String>()
        
        dict["key"] = apiKey
        dict["Country"] = country ?? ""
        if let _date = date
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy"
            dict["StartDate"] = dateFormatter.string(from: _date)
        }
        dict["DaysMin"] = daysfrom ?? ""
        dict["DaysMax"] = daysto ?? ""
        dict["BudgetMin"] = pricefrom ?? ""
        dict["BudgetMax"] = priceto ?? ""
            
        dict["Name"] = nameTextField.text
        dict["Phone"] = phoneTextField.text
        dict["EMail"] = nameTextField.text ?? ""
        dict["Comment"] = nameTextField.text ?? ""
        
        sendPostJSONRequest("https://mtours.ru/api/mobile/orderassortmenttours.json", JSONDictionary: dict as Dictionary<String, AnyObject>)
    }
}
