//
//  RequestViewController.swift
//  tuiapp
//
//  Created by ifau on 27/11/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import AddressBook
import AddressBookUI

class RequestViewController: UIViewController
{
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var messageTextView: IQTextView!
    
    @IBOutlet var contentView: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        initialize()
    }
    
    fileprivate func initialize()
    {
        nameTextField.layer.borderColor = Constants.blueColor().cgColor
        nameTextField.layer.borderWidth = 1
        nameTextField.backgroundColor = UIColor.clear
        nameTextField.layer.cornerRadius = 2
        nameTextField.delegate = self
        
        phoneTextField.layer.borderColor = Constants.blueColor().cgColor
        phoneTextField.layer.borderWidth = 1
        phoneTextField.backgroundColor = UIColor.clear
        phoneTextField.layer.cornerRadius = 2
        phoneTextField.delegate = self
        
        emailTextField.layer.borderColor = Constants.blueColor().cgColor
        emailTextField.layer.borderWidth = 1
        emailTextField.backgroundColor = UIColor.clear
        emailTextField.layer.cornerRadius = 2
        emailTextField.delegate = self
        
        messageTextView.layer.borderColor = Constants.blueColor().cgColor
        messageTextView.layer.borderWidth = 1
        messageTextView.backgroundColor = UIColor.clear
        messageTextView.layer.cornerRadius = 2
        
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RequestViewController.hideKeyboard))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
    
    func submitButtonPressed(_ sender: AnyObject)
    {
        hideKeyboard()
    }
    
    func hideKeyboard()
    {
        view.endEditing(true)
    }
    
    fileprivate var loadingImageView: UIImageView?
    
    func showLoadingIndicator()
    {
        guard loadingImageView == nil else
        {
            return
        }
        
        loadingImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 160, height: 160))
        loadingImageView!.image = UIImage(named: "loading")
        loadingImageView!.center = self.view.center
        self.view.addSubview(loadingImageView!)
        
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
        
        rotationAnimation.fromValue = 0.0
        rotationAnimation.toValue = Float(M_PI * 2.0)
        rotationAnimation.duration = 2
        rotationAnimation.repeatCount = Float.infinity
        
        loadingImageView!.layer.add(rotationAnimation, forKey: "loadingRotation")
    }
    
    func hideLoadingIndicator()
    {
        guard loadingImageView != nil else
        {
            return
        }
        
        loadingImageView!.layer.removeAnimation(forKey: "loadingRotation")
        loadingImageView!.removeFromSuperview()
        loadingImageView = nil
    }
    
    func showEmptyFieldsError()
    {
        let messageLabel = UILabel(frame: view.frame)
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.boldSystemFont(ofSize: 14)
        messageLabel.textColor = Constants.orangeColor()
        messageLabel.text = "Поля Имя и Телефон не заполнены"
        messageLabel.alpha = 0
        view.addSubview(messageLabel)
        
        self.contentView.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.5, animations: {
            
            self.contentView.alpha = 0
            messageLabel.alpha = 1
        },
        completion: { (success: Bool) in
                                    
            UIView.animate(withDuration: 0.5, delay: 3, options:[], animations: {
                                        
                self.contentView.alpha = 1
                messageLabel.alpha = 0
            },
            completion: { (success: Bool) in
                                            
                self.contentView.isUserInteractionEnabled = true
                messageLabel.removeFromSuperview()
            })
        })
    }
    
    func showError()
    {
        let messageLabel = UILabel(frame: view.frame)
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.boldSystemFont(ofSize: 14)
        messageLabel.textColor = Constants.orangeColor()
        messageLabel.text = "Произошла ошибка :(\n\nПопробуйте еще раз!"
        messageLabel.alpha = 0
        view.addSubview(messageLabel)
        
        self.contentView.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.5, animations: {
            
            self.contentView.alpha = 0
            messageLabel.alpha = 1
        },
        completion: { (success: Bool) in
            
            UIView.animate(withDuration: 0.5, delay: 3, options:[], animations: {
                
                    self.contentView.alpha = 1
                    messageLabel.alpha = 0
                },
                completion: { (success: Bool) in
                    
                    self.contentView.isUserInteractionEnabled = true
                    messageLabel.removeFromSuperview()
                })
        })
    }
    
    func showSuccess()
    {
        let messageLabel = UILabel(frame: view.frame)
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.boldSystemFont(ofSize: 14)
        messageLabel.textColor = Constants.orangeColor()
        messageLabel.text = "Спасибо за обращение!\n\nВаша заявка уже в работе.\n\nМы свяжимся с вами в ближайшее время :)"
        messageLabel.alpha = 0
        view.addSubview(messageLabel)
        
        self.contentView.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.5, animations: {
            
            self.contentView.alpha = 0
            messageLabel.alpha = 1
        })
    }
    
    func sendPostJSONRequest(_ urlString: String, JSONDictionary: Dictionary<String, AnyObject>)
    {
        let session = URLSession.shared
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        let data = try! JSONSerialization.data(withJSONObject: JSONDictionary, options: [])
        
        request.httpBody = data
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        self.showLoadingIndicator()
        showStatusBarLoadingIndicator()
        
        let task = session.dataTask(with: request)
        { [unowned self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            self.hideLoadingIndicator()
            hideStatusBarLoadingIndicator()
            
            if error == nil && data != nil
            {
                DispatchQueue.main.async(execute: { self.showSuccess() })
            }
            else
            {
                DispatchQueue.main.async(execute: { self.showError() })
            }
        }
        
        task.resume()
    }
}

extension RequestViewController: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField === nameTextField
        {
            phoneTextField.becomeFirstResponder()
        }
        else if textField === phoneTextField
        {
            emailTextField.becomeFirstResponder()
        }
        else if textField === emailTextField
        {
            messageTextView.becomeFirstResponder()
        }
        return false
    }
}

extension RequestViewController: ABPeoplePickerNavigationControllerDelegate
{
    @IBAction func openContactsPicker(_ sender: AnyObject)
    {
        let contactPicker = ABPeoplePickerNavigationController()

        ABAddressBookRequestAccessWithCompletion(contactPicker, { success, error in
            
            if success
            {
                contactPicker.peoplePickerDelegate = self
                self.present(contactPicker, animated: true, completion: nil)
            }
            else
            {
                let alertController = UIAlertController(title: "Нет доступа", message: "Необходимо разрешить доступ к адресной книге в настройках iOS", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                    alertController.dismiss(animated: true, completion: nil)
                })
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
        })
    }
    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController, didSelectPerson person: ABRecord)
    {
        let nameCFString : CFString = ABRecordCopyCompositeName(person).takeRetainedValue()
        let name : NSString = nameCFString as NSString
        nameTextField.text = name as String
        
        let phones: ABMultiValue = ABRecordCopyValue(person, kABPersonPhoneProperty).takeRetainedValue()
        if (ABMultiValueGetCount(phones) > 0)
        {
            let index = 0 as CFIndex
            let phone = ABMultiValueCopyValueAtIndex(phones, index).takeRetainedValue() as! String
            phoneTextField.text = phone
        }
        
        let emails: ABMultiValue = ABRecordCopyValue(person, kABPersonEmailProperty).takeRetainedValue()
        if (ABMultiValueGetCount(emails) > 0)
        {
            let index = 0 as CFIndex
            let email = ABMultiValueCopyValueAtIndex(emails, index).takeRetainedValue() as! String
            emailTextField.text = email
        }
    }
}
