//
//  AboutAviumViewController.swift
//  hittelecom
//
//  Created by ifau on 05/07/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class AboutAviumViewController: UIViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        addCloseButton()
    }
    
    func addCloseButton()
    {
        let closeButton = UIBarButtonItem(title: "Закрыть", style: .plain, target: self, action: #selector(AboutAviumViewController.closeAction))
        navigationItem.leftBarButtonItem = closeButton
    }
    
    func closeAction()
    {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func webButtonPressed(sender: AnyObject)
    {
        UIApplication.shared.openURL(NSURL(string: "http://avium.ru")! as URL)
    }
    
    @IBAction func mailButtonPressed(sender: AnyObject)
    {
        UIApplication.shared.openURL(NSURL(string: "mailto:info@avium.ru")! as URL)
    }
    
    @IBAction func locationButtonPressed(sender: AnyObject)
    {
        UIApplication.shared.openURL(NSURL(string: "http://maps.apple.com/?ll=55.937982,37.862013&z=16")! as URL)
    }
}
